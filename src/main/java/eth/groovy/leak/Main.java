package eth.groovy.leak;

import eth.groovy.leak.runnable.PrintRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[]args) {
        logger.info(">>>>> Start Groovy leak test");
        Thread t = null;
        int threadCnt = 10;
        int threadSleepTime = 400;
        while(true) {
            try {
                for(int i=0; i<threadCnt; i++) {
                    t = new Thread(new PrintRunnable());
                    t.start();
                }
                Thread.sleep(threadSleepTime);
            } catch (Exception e) {
                logger.error(">>>> Exception was occurred by this reason =>" , e);
            }
        }
    }
}

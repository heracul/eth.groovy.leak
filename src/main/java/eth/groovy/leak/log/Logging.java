package eth.groovy.leak.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Logging {
    private Logger logger = LoggerFactory.getLogger(Logging.class);
    public String converArrayToString(String ... strs) {
        StringBuilder sb = new StringBuilder();

        for(String text : strs) {
            sb.append("It's a ").append(text).append(".\n");
        }
        return sb.toString();
    }

    public void logging(String fullText) {
        logger.debug(fullText);
    }
}

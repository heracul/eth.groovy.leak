package eth.groovy.leak.runnable;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class PrintRunnable implements Runnable {

    private Logger logger = LoggerFactory.getLogger(PrintRunnable.class);
    private GroovyScriptEngine gse = null;
    private final String SPOOL_PATH = "C:\\projects\\ethan_projects\\eth.groovy.leak\\spool\\";
    private final String ORI_GROOVY_FILE_PATH = "C:\\projects\\ethan_projects\\eth.groovy.leak\\script\\GroovyHandler.groovy";
    public PrintRunnable () {
        try {
            gse = new GroovyScriptEngine(SPOOL_PATH);
        } catch (IOException e) {
            logger.error("Can't generate instance by this reason =>" ,e);
        }
    }

    @Override
    public void run() {
        Binding binding= new Binding();
        long startDttm = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        sb.append("GroovyHandler_")
                .append(Thread.currentThread().getName())
                .append("_")
                .append(startDttm)
                .append(".groovy");
        cloneFile(ORI_GROOVY_FILE_PATH, SPOOL_PATH+sb.toString());
        try {
            Object rtnObj = gse.run(sb.toString(), binding);
        } catch (ResourceException e) {
            e.printStackTrace();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        deleteFile(SPOOL_PATH+sb.toString());
    }


    private void cloneFile(String oriFile, String spoolPath) {
        try {
            FileUtils.copyFile(new File(oriFile), new File(spoolPath));
        } catch (IOException e) {
            logger.error("Fail to copy file =>", e);
        }
    }

    private void deleteFile(String filePath) {
        File file = new File(filePath);
        file.delete();
    }
}
